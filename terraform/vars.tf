variable "RDS_PASSWORD" {
  default = "somepassword"
}

variable "API_PORT" {
  default = "5432"
}

variable "WEB_PORT" {
  default =  "8080"
}

variable "AWS_REGION" {
  default = "ap-northeast-2"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "mykey"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "mykey.pub"
}
/*
variable "API_INSTANCE_AMI" {
  default = "ami-070e38dab88a4f4a2"
}

variable "WEB_INSTANCE_AMI" {
  default = "ami-0cdff90a7642a9997"
}*/


variable "AMIS" {
  type = "map"
  default = {
    "ap-northeast-2" = "ami-0ba5cd124d7a79612"
  }
}

variable "INSTANCE_USERNAME" {
  default = "ubuntu"
}