terraform {
  backend "s3" {
    bucket = "node-aws-jenkins-hit-terraform"
    key = "master-init/ap-northeast-2/dev/node-aws-jenkins-nodehit-terraform.tfstate"
    region = "ap-northeast-2"
  }
}